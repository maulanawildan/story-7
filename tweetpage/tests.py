from django.test import TestCase
from django.test import Client
from django.test import LiveServerTestCase
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
import unittest
import time


class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()


class TweetTest(TestCase):

    def test_navbar_is_exist(self):
        self.response = Client().get('')
        self.assertIn('</nav>', self.response.content.decode())

    def test_profil_page_is_exist(self):
        self.response = Client().get('')
        self.assertEqual(self.response.status_code, 200)

    def test_photo_is_exist(self):
        self.response = Client().get('')
        self.assertIn('<img src="../static/wildan.jpg" class="rounded" style="width: 15rem; height: 10rem;" alt="Foto Profil">', self.response.content.decode())

    def test_accordion_is_exist(self):
        self.response = Client().get('')
        self.assertIn('<button class="accordion">Aktivitas</button>', self.response.content.decode())

    def test_accordion_panel_is_exist(self):
        self.response = Client().get('')
        self.assertIn('<div class="panel">', self.response.content.decode())

    def test_profil_page_is_not_exist(self):
        self.response = Client().get('error/')
        self.assertEqual(self.response.status_code, 404)
